run:
	docker-compose up --build

up:
	docker-compose up

rebuild:
	docker-compose build --no-cache

build:
	docker-compose build

restart:
	docker-compose restart

down:
	docker-compose down

test:
	docker-compose up test

prune:
	docker system prune -af
