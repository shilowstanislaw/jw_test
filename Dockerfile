FROM python:3.8

ENV PYTHONUNBUFFERED 1

RUN mkdir /code
WORKDIR /code
COPY requirements.txt /code/
COPY api_test/test_requirements.txt /code/

RUN pip install -r requirements.txt
RUN pip install -r test_requirements.txt

COPY . /code/
