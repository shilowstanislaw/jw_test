import itertools
from django import forms
from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from django.contrib.admin.widgets import FilteredSelectMultiple
from content_page_type.models import Audio, Text, Video
from .models import Page

class PageAdmin(admin.ModelAdmin):
	pass

admin.site.register(Page, PageAdmin)
