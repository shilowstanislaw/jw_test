from django.db import models
from django.urls import reverse
from django.utils import timezone
from content_page_type.fields import CounterField 
# Create your models here.

class Page(models.Model):

	title = models.CharField(
			max_length=1000,
			blank=True,
			verbose_name='заголовок'
		)
	
	slug = models.SlugField(
			unique=True,
			db_index=False,
			verbose_name='client url'
		)
	
	created_date = models.DateTimeField(
			default=timezone.now,
			verbose_name='дата создания'
		)
	
	cache_count = CounterField()
	
	view_count = models.PositiveIntegerField(
			default=0,
			verbose_name='счетчик просмотров'
		)
	class Meta:
		default_related_name = 'pages'
	
	def current_views_count(self):
		# сумируем сохраненное и кешируемое значение
		return self.view_count + int(self.cache_count.get_score())
	
	current_views_count.short_description = "Текущее число просмотров (Новый счётчик)"

	current_views_count = property(current_views_count)
	
	def get_absolut_url(self):
		return reverse('page_detail', kwargs={'id': self.id})

	def __str__(self):
		return f'{self.title}'