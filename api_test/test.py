import os
import yaml
import asyncio
import aiohttp
import ujson
import time 
import pytest
import socket


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

    colors_status = {
        200: OKBLUE,
        404: WARNING,
        400: WARNING,
        500: FAIL
    }

BASE_DIR = os.path.dirname(os.path.abspath(__file__))


with open(BASE_DIR+'/test-api.yml') as file:
    
    default_data = yaml.load(file, Loader=yaml.FullLoader)
    
    BASE_API_URL = default_data['api']['base_url']
    
    ALL_PAGES_URL = default_data['api']['all_pages'] + "{}"
    DETAIL_PAGE_URL = default_data['api']['detail_page'] + "/{}"

async def test_many_deetail_page(
    session, url, params={},
    start=time.time(),
    end=time.time,
    *args, **kwargs
    ):
    # Проверка нагрузки на сервер
    async with session.get(url, json=params) as resp:
        resp_time_delta = end() - start        
        print(f"""Api Test {
                bcolors.UNDERLINE}{url}{bcolors.ENDC}: {
                bcolors.colors_status[resp.status]
                } response status {resp.status} (OK):{
                resp_time_delta } msec{bcolors.ENDC
            }\n""")
        print(await resp.json(), "\n")


async def test_many_data_types(
    session, url, params={},
    start=time.time(),
    end=time.time,
    *args, **kwargs
    ):
    # проверка типа данных ответа от сервера
    async with session.get(url, json=params) as resp:
        resp_time_delta = end() - start
        
        print(f"""Api Test {
                bcolors.UNDERLINE}{url}{bcolors.ENDC}: {
                bcolors.colors_status[resp.status]
                } response status {resp.status} (OK):{
                resp_time_delta } msec{bcolors.ENDC
            }\n""")
        response = await resp.json()
        
        assert type(response) == dict
        print(f'response type -> dict {bcolors.OKBLUE}(OK){bcolors.ENDC}')
        
        assert type(response['data']) == list
        print(f'response.data type -> list (array) {bcolors.OKBLUE}(OK){bcolors.ENDC}')

@pytest.mark.asyncio
async def test_detail_data_types(
    session, url, params={},
    start=time.time(),
    end=time.time,
    *args, **kwargs
    ):
    # проверка типа данных ответа от сервера
    async with session.get(url, json=params) as resp:
        resp_time_delta = end() - start
        
        print(f"""Api Test {
                bcolors.UNDERLINE}{url}{bcolors.ENDC}: {
                bcolors.colors_status[resp.status]
                } response status {resp.status} (OK):{
                resp_time_delta } msec{bcolors.ENDC
            }\n""")
        response = await resp.json()
        
        assert type(response) == dict
        print(f'api response type -> dict: {bcolors.OKBLUE}(OK){bcolors.ENDC}')
        
        assert type(response['data']) == list
        print(f'api response.data type -> list (array): {bcolors.OKBLUE}(OK){bcolors.ENDC}')
        
        assert type(response['data'][0]['video_related']) == list
        print(f'api response.data[0].video_related type -> list (array): {bcolors.OKBLUE}(OK){bcolors.ENDC}')

        assert type(response['data'][0]['text_related']) == list
        print(f'api response.data[0].text_related type -> list (array): {bcolors.OKBLUE}(OK){bcolors.ENDC}')

        assert type(response['data'][0]['audio_related']) == list
        print(f'api response.data[0].audio_related type -> list (array):" {bcolors.OKBLUE}(OK){bcolors.ENDC}')

@pytest.mark.asyncio
async def test_substring_searсh(
    session, url, params={},
    start=time.time(),
    end=time.time,
    *args, **kwargs
    ):
    # проверка типа данных ответа от сервера
    async with session.get(url, json=params) as resp:
        resp_time_delta = end() - start
        
        print(f"""Api Test {
                bcolors.UNDERLINE}{url}{bcolors.ENDC}: {
                bcolors.colors_status[resp.status]
                } response status {resp.status} (OK):{
                resp_time_delta } msec{bcolors.ENDC
            }\n""")
        
        response = await resp.json()
        print(response['data'][0])
        assert kwargs['str'].lower() in response['data'][0]['title'].lower()
        print(f'api response substring searсh title is valid:{bcolors.OKBLUE} (OK){bcolors.ENDC}')
    
async def fetch_concurrent(urls, method, *args, **kwargs):
    
    loop = asyncio.get_event_loop()
    
    async with aiohttp.ClientSession(
                connector=aiohttp.TCPConnector(
                    family=socket.AF_INET,
                    verify_ssl=False,
                    limit=1, 
                ),
                json_serialize=ujson.dumps
        ) as session:
        tasks = []
        
        for url in urls:
            tasks.append(loop.create_task(method(session=session, url=url, *args, **kwargs)))

        for result in asyncio.as_completed(tasks):
            response = await asyncio.gather(result)
            

def test_many_request(r_count=100):
    asyncio.run(fetch_concurrent(
        [BASE_API_URL + DETAIL_PAGE_URL.format(i) for i in range(1, 100)],
        test_many_deetail_page
        )
    )

def test_many_types(r_count=2):
    asyncio.run(fetch_concurrent(
        [BASE_API_URL + ALL_PAGES_URL.format('') for i in range(1, r_count)],
        test_many_data_types)
    )

def test_detail_types(r_count=2):
    asyncio.run(fetch_concurrent(
        [BASE_API_URL + DETAIL_PAGE_URL.format(i) for i in range(1, r_count)],
        test_detail_data_types)
    )

def test_substrsearсh(_str='кон', r_count=2):
    asyncio.run(fetch_concurrent(
        [BASE_API_URL + ALL_PAGES_URL.format('?title__icontains='+_str) for i in range(1, r_count)],
        test_substring_searсh,
        str=_str)
    )

test_many_request()
test_many_types()
test_detail_types()
test_substrsearсh()


