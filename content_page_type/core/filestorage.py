import datetime
import os
from mutagen.mp3 import MP3


def video_directory_path(instance, filename):
    """
    video upload directory setting
    videos/{directed_by}/{filename}
    """
    return "videos/{directed_by}/{filename}".format(
        directed_by=instance.directed_by,
        filename=filename,
    )

def subtitle_directory_path(instance, filename):
    """
    subtitle upload directory setting 
    videos/{directed_by}/subtitles/{filename}
    """
    return "videos/{directed_by}/subtitles/{filename}".format(
        directed_by=instance.video.directed_by,
        filename=filename,
    )

def audio_directory_path(instance, filename):
    """
    audio upload directory setting
    e.g
        audio/{author}/{filename}
    """
    return "audio/{author}/subtitles/{filename}".format(
        author=instance.author,
        filename=filename,
    )

def bitrate_file(file):
    """
    audio bitrate (bit / sec)
    """
    f = MP3(file)
    return f.info.bitrate / 1000
