from datetime import datetime
from django_redis import get_redis_connection
from redis import WatchError 


class Counter:
	"""
	Модель счетчика для любой модели
	Храним просмотры в Redis

	Имя сохраняемой модели берется из 
	"""

	def __init__(self, obj, obj_type, scheme=None):
		self.obj = obj
		self.obj_type = obj_type
		self.connection = self._get_connection()

	@staticmethod
	def _get_connection(alias="default"):
		return get_redis_connection(alias)
	
	def _save_views(self, pipe):
		self.obj.view_count = self.obj.view_count + self.get_score()
		self.obj.save()
	
	def increment(self, value=1, date=None):
		"""Increment score for all periods of scheme. Provided to redis zincrby."""
		# date = date or datetime.now()
		with self.connection.pipeline() as pipe:
			pipe.multi()
			pipe.zincrby(name=self.obj_type.__name__, amount=value, value=self.obj.pk)
			pipe.execute()
	
	def set(self, value=1, date=None):
		""" помещаем id и значение просмотра и в сортед сет по имени объекта """
		# date = date or datetime.now()
		with self.connection.pipeline() as pipe:
			pipe.multi()
			pipe.zadd(self.obj_type.__name__, {self.obj.pk: value})
			pipe.execute()
 	
	def remove_to_db(self, date=None):
		# date = date or datetime.now()
		with self.connection.pipeline() as pipe:
			pipe.multi()
			self._save_views(pipe)
			pipe.zrem(self.obj_type.__name__, self.obj.pk)
			pipe.execute()
			
	
	def get_score(self, *args, **kwargs):
		if self.obj.pk:
			score = self.connection.zscore(self.obj_type.__name__, self.obj.pk)
		else:
			score = 0
		return int(score or 0)


class CounterField:
	"""
	Wrapper for Counter.
	This is not Django Model Field because Counter not provide data to default database.
	"""

	def __init__(self, scheme=None, verbose_name='CounterField'):
		self.verbose_name = verbose_name

	def __get__(self, obj, obj_type):
		return Counter(obj, obj_type)
