from django.db import models
from .core.filestorage import (
	video_directory_path,
	subtitle_directory_path,
	audio_directory_path,
	bitrate_file
	)
from .mixins import ContentMixin


class SubTitles(models.Model):
	video = models.ForeignKey(
			'Video',
			on_delete=models.CASCADE,
			null=True,
			blank=True,
			verbose_name='субтитры',
			related_name='video_subtitle',
			db_index=False
		)
	
	file = models.FileField(
			upload_to=subtitle_directory_path,
			blank=True,
			verbose_name='субтитры'
		)
	
	lang = models.CharField(
			max_length=40,
			verbose_name='Язык'
		)
	class Meta:
		unique_together = ("video", "lang")
	
	def __str__(self):
		return f'{self.video.title}: яз. {self.lang}'

class Video(ContentMixin):
	
	directed_by = models.CharField(
			max_length=200,
			blank=True,
			verbose_name='режиссер'
		)
			
	file = models.FileField(
			upload_to=video_directory_path,
			verbose_name='файл',
			blank=True,
		)
	
	def __str__(self):
		return f'{self.title}'

class Audio(ContentMixin):
	
	file = models.FileField(
			upload_to=audio_directory_path,
			verbose_name='файл'
		)
	
	cover = models.ImageField(
			upload_to='audios/',
			verbose_name='обожка'
		)
	
	author = models.CharField(
			max_length=500,
			blank=True,
			verbose_name='автор'
		) 
	
	def bitrate(self, *args, **kwargs):
		return bitrate_file(self.file)
		
	bitrate = property(bitrate)
	
	def __str__(self):
		return f'{self.title}, {self.author}'


class Text(ContentMixin):

	author = models.CharField(
			max_length=500,
			blank=True,
			verbose_name='автор'
		) 
	
	def __str__(self):
		return f'{self.title}, {self.author}'


