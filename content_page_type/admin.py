from django.contrib import admin
from .models import Audio,  Video, Text, SubTitles

class SubTitlesAdmin(admin.ModelAdmin):
	pass

class AudioContentAdmin(admin.ModelAdmin):
	filter_horizontal = ['pages']


class SubTitlesline(admin.StackedInline):
	model = SubTitles

class VideoContentAdmin(admin.ModelAdmin):
	filter_horizontal = ['pages']
	inlines = [SubTitlesline,]


class TextContentAdmin(admin.ModelAdmin):
	filter_horizontal = ['pages']


admin.site.register(Audio, AudioContentAdmin)
admin.site.register(Video, VideoContentAdmin)
admin.site.register(Text, TextContentAdmin)
admin.site.register(SubTitles, SubTitlesAdmin)

