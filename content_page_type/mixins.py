from django.utils import timezone
from django.db import models
from .fields import CounterField

class ContentMixin(models.Model):

	
	title = models.CharField(
			max_length=1000,
			verbose_name='заголовок'
		)
	
	text = models.TextField(
			verbose_name='содержание',
			null=True,
			blank=True 
		)
	
	created_date = models.DateTimeField(
			default=timezone.now,
			verbose_name='дата создания'
		)
	
	cache_count = CounterField()
	
	view_count = models.PositiveIntegerField(
			default=0,
			verbose_name='счетчик просмотров'
		)
	
	cover = models.ImageField(
			upload_to='covers/',
			verbose_name='обожка'
		)
	
	pages = models.ManyToManyField(
		'page.Page',
		related_name="%(class)s_related",
		related_query_name="%(class)ss",
		)

	def current_views_count(self):
		return self.view_count + int(self.cache_count.get_score())
	
	current_views_count.short_description = "Текущее число просмотров (Новый счётчик)"

	current_views_count = property(current_views_count)
	
	class Meta:
		abstract = True
		ordering = ['created_date']
