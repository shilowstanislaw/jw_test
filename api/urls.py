from django.urls import path
from .views.api import(
        PagesView,
        PageDetailView
    )

urlpatterns = [
    path('pages', PagesView.as_view()),
    path('page/<int:id>', PageDetailView.as_view(), name='page_detail')
]