import time
from just_work_test.celery import app
from page.models import Page
#from django.db import transaction

def write_related_view(query):
	for obj in query.iterator():
		obj.cache_count.increment()
		if bool(obj.cache_count.get_score() > 100):
			obj.cache_count.remove_to_db()

@app.task
def write_view_page(*args, **kwargs):
	try:
		obj = Page.objects.get(id=kwargs['id'])
	
	except Page.DoesNotExist:
		return None

	time.sleep(0.5)
	
	obj.cache_count.increment()
	
	if obj.cache_count.get_score() > 100:
		obj.cache_count.remove_to_db()
	
	if bool(obj.video_related.count()):
		write_related_view(obj.video_related)
	
	if bool(obj.audio_related.count()):
		write_related_view(obj.audio_related)
	
	if bool(obj.text_related.count()):
		write_related_view(obj.text_related)


