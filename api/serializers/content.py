from .core import get_autual_count
from rest_framework import serializers
from content_page_type.models import Video, Text, Audio


class VideoContentSerializer(serializers.ModelSerializer):
	
	count = serializers.SerializerMethodField('view_count_get')
	
	class Meta:
		model = Video
		fields = (
				'id',
				'title',
				'file',
				'cover',
				'directed_by',
				'count',
				'created_date'
			)

	def view_count_get(self, obj):
		return obj.current_views_count

class TextConentSerializer(serializers.ModelSerializer):
	
	count = serializers.SerializerMethodField('view_count_get')
	
	class Meta:
		model = Text
		fields = (
				'id',
				'title',
				'text',
				'count'
				'created_date'
			)
	
	def view_count_get(self, obj):
		return obj.current_views_count

class AudioContentSerializer(serializers.ModelSerializer):
	
	count = serializers.SerializerMethodField('view_count_get')
	bitrate = serializers.SerializerMethodField('bitrate_file')
	
	class Meta:
		model = Audio
		fields = (
				'id',
				'title',
				'file',
				'bitrate',
				'author',
				'cover',
				'count',
				'created_date'
			)
	
	def view_count_get(self, obj):
		return obj.current_views_count

	def bitrate_file(self, obj):
		return obj.bitrate

