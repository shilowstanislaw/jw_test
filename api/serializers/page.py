from page.models import Page
from .core import get_autual_count
from rest_framework import serializers
from .content import AudioContentSerializer, TextConentSerializer, VideoContentSerializer

	
class PageSerializer(serializers.ModelSerializer):
	
	api_url = serializers.SerializerMethodField(
			'api_get_url'
		)	
	
	class Meta:
		model = Page
		fields = (
				'id',
				'title',
				'slug',
				'api_url',

			)

	def api_get_url(self, obj):
		return f'api/v1/pages/{obj.id}/'


class PageDetailSerializer(serializers.ModelSerializer):
	count = serializers.SerializerMethodField('view_count_get')
	video_related = VideoContentSerializer(many=True)
	audio_related = AudioContentSerializer(many=True)
	text_related = TextConentSerializer(many=True)
	
	class Meta:
		model = Page
		fields = (
				'id',
				'title',
				'slug',
				'video_related',
				'audio_related',
				'text_related',
				'count',
				'created_date'
			)

	def view_count_get(self, obj):
		return obj.current_views_count