from .base import ApiView
from api.tasks import write_view_page
from page.models import Page
from django.db import transaction
from api.serializers import (PageSerializer, PageDetailSerializer)
from just_work_test.core import query_debugger


class PagesView(ApiView):
	
	def __init__(self, *args, **kwargs):
		super(PagesView, self).__init__(*args, **kwargs)
		self.model = Page
		self.serializer = PageSerializer
		self.query_method = self.model.objects.filter
		self.args = args
		self.kwargs = kwargs

	@query_debugger
	def get(self, request,  *args, **kwargs):
		super().get(request, *args, **kwargs)
		self.data = self.serialize(*args, **kwargs)
		return self.api_response()


class PageDetailView(PagesView):

	@query_debugger
	def get(self, request, *args, **kwargs):
		super().get(request, *args, **kwargs)
		self.serializer = PageDetailSerializer
		self.query_kwargs.update(kwargs)
		transaction.on_commit(lambda: write_view_page.delay(**kwargs))
		self.data = self.serialize(*args, **kwargs)
		return self.api_response()
