import os
import datetime
from django.conf import settings
from django.http import JsonResponse
from abc import ABCMeta, abstractmethod
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.parsers import (
    FileUploadParser, 
    MultiPartParser,
    FormParser,
    JSONParser
    )
from rest_framework.renderers import JSONRenderer
from django.core.files.storage import default_storage


class Api(metaclass=ABCMeta):

    
    @abstractmethod
    def get(self, request):
        """ базовый метод GET API.
        По умолчанию возвращает
        405 - неподдерживаемый метод
        
        Args:
            request
        """
        self.method = request.method
        request.GET._mutable = True
        return JsonResponse({
            'msg':  self.RESPONSE_MSG},
            safe=False, status=405,
            json_dumps_params={'ensure_ascii':False})
    
    @abstractmethod    
    def post(self, request):
        """ базовый метод  POST API.
        По умолчанию возвращает
        405 - неподдерживаемый метод
        
        Args:
            request
        """
        self.method = request.method
        request.data._mutable = True
        return JsonResponse({
            'msg':  self.RESPONSE_MSG},
            safe=False, status=405,
            json_dumps_params={'ensure_ascii':False})
    
    @abstractmethod
    def options(self, request):
        """ базовый метод OPTIONS API.
        По умолчанию возвращает
        405 - неподдерживаемый метод
        
        Args:
            request
        """
        self.method = request.method
        return JsonResponse({
            'msg':  self.RESPONSE_MSG},
            safe=False, status=405,
            json_dumps_params={'ensure_ascii':False})
    
    @abstractmethod
    def put(self, request):
        """ базовый метод PUT API.
        По умолчанию возвращает
        405 - неподдерживаемый метод
        
        Args:
            request
        """
        self.method = request.method
        request.data._mutable = True
        return JsonResponse({
            'msg':  self.RESPONSE_MSG},
            safe=False, status=405,
            json_dumps_params={'ensure_ascii':False})
    
    @abstractmethod
    def delete(self, request):
        """ базовый метод DELETE API.
        По умолчанию возвращает
        405 - неподдерживаемый метод
        
        Args:
            request
        """
        self.method = request.method
        request.data._mutable = True
        return JsonResponse({
            'msg':  self.RESPONSE_MSG},
            safe=False, status=405,
            json_dumps_params={'ensure_ascii':False})

    def bool_param(self, param, data):
        return bool(param == 'true') if param in data else True


class BaseAPI(APIView, Api):
    
    RESPONSE_MSG = "Unsupported Method. This request don't support this http method."
    ERROR_URL_METHOD = ' отправлено неверное значение'
    SUCCESS_CREATE_MSG = ' данные успешно сохранены'
    SUCCESS_UPDATE_MSG = ' данные успешно обновлены'
    SUCCESS_DELETE_MSG = ' данные успешно удалены'
    SUCCESS_GET_MSG = ' успешно'
    RESPONSE_DEFAULT_MSG = ' что-то пошло не так, повторите попытку позже'
    NONE_DEL_DATA = ' данные невозможно удалить'
    NULL_FALSE_FIELD = ' не заполнены обязательные поля'
    SOMETHING_WRONG = ' что-то пошло не так'
    FOURZEROFOUR = ' данные не найдены'
    
    ALL_RESPONSE_MESAGES = {
            'GET': SUCCESS_GET_MSG,
            'POST': SUCCESS_CREATE_MSG,
            'PUT': SUCCESS_UPDATE_MSG,
            'DELETE': SUCCESS_DELETE_MSG,
        }

    IN_RESP_OBJ = {
            'msg': RESPONSE_DEFAULT_MSG
        }
    
    exceptions_dict = {
            404: FOURZEROFOUR,
            400: SOMETHING_WRONG,
            409: NULL_FALSE_FIELD,
            406: NONE_DEL_DATA
        }
    

    
class ApiView(BaseAPI):
    """ Базовый API """
    
    parser_classes = (
        FormParser,
        JSONParser,
        MultiPartParser
    )
    
    renderer_classes = [JSONRenderer]
    
    def __init__(self, *args, **kwargs):
        self.data = None
        self.status = 400
        self.exception = None 
        self.method = None
        self.serializer = None
        self.model = None
        self.query_method = None
        self.model_count = None
        self.query_kwargs = {}
        self.order = '-created_date'
        self.paginate = (None, None)
        self.msg = lambda msg: msg if msg != None else self.exceptions_dict.get(self.status) \
            if self.status in self.exceptions_dict.keys() else \
                self.ALL_RESPONSE_MESAGES.get(self.method)
        

    def get(self, request, *args, **kwargs):
        super().get(request)

        if 'page' and 'part' in request.GET:
            self.api_get_paginate(
                    part=request.GET.pop('part'),
                    page=request.GET.pop('page'),
                )
        if 'order_by' in request.GET:
            self.order = request.GET.pop('order_by')
        
        if 'title__icontains' in request.GET:
            self.query_kwargs = {'title__icontains': request.GET.get("title__icontains")}
    
    def post(self, request):
        super().post(request)

    def options(self, request):
        super().options(request)

    def put(self, request):
        super().put(request)

    def delete(self, request):
        super().delete(request)
        request.data['success_del'] = request.data['success_del'] == 'true'

    def api_get_paginate(self, part, page):
        part = int(part[0])
        page = int(page[0])
        marker = (part * page) - part
        self.paginate = [marker, marker + part] if (page + part) > 2 else [None, None]
        
    def request_serializer(self, request, params):
        # вспомогательный метод,
        # юзаем для того, что-б убрать из реквест даты
        # лишние параметры если нужно
        for k, v in params.items():
            if k in request.GET.keys():
                    request.GET[k] = v
        return request.GET
    
    def serialize(self, many=True, *args, **kwargs):
        # сериализуем множество
        # вызываем из интерфеса дочернего класса
        # если параметры реквета не содержат аргуметов
        self.model_count = self.query_method(**self.query_kwargs).order_by(
                    self.order
                ).count()

        return self.serializer(
                self.query_method(**self.query_kwargs).order_by(
                    self.order
                )[self.paginate[0]: self.paginate[1]], many=many
            ).data
    
    def api_response(self):
        if self.exception:
            self.IN_RESP_OBJ['msg'] = self.exception
        
        if self.data != None :
            self.status = 200 if bool(len(self.data)) else 404
            self.IN_RESP_OBJ['msg'] = self.ALL_RESPONSE_MESAGES.get(self.method)
            self.IN_RESP_OBJ['data'] = self.data
            self.IN_RESP_OBJ['len'] = self.model_count
        
        return JsonResponse(
                    self.IN_RESP_OBJ,
                    status=self.status,
                    safe=False,
                    json_dumps_params={'ensure_ascii': False}
                )
