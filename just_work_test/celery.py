import os
from celery import Celery
 
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'just_work_test.settings')
 
app = Celery('just_work_test')
app.config_from_object('django.conf:settings')
 
# Load task modules from all registered Django app configs.
app.autodiscover_tasks()