"""
WSGI config for just_work_test project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/2.0/howto/deployment/wsgi/
"""

import os

from django.core.wsgi import get_wsgi_application
from django.db.backends.signals import connection_created
from django.dispatch import receiver

@receiver(connection_created)
def setup_postgres(connection, **kwargs):
    if connection.vendor != 'postgresql':
        return
    
    # Тайм-аут через минуту.
    with connection.cursor() as cursor:
        cursor.execute("""
            SET statement_timeout TO 60000;
        """)

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "just_work_test.settings")

application = get_wsgi_application()
